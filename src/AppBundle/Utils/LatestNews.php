<?php

namespace AppBundle\Utils;

class LatestNews
{
    private $url = 'https://api.tibiadata.com/v2/latestnews.json';

    public function request() : string
    {
        return file_get_contents($this->url);
    }
}
