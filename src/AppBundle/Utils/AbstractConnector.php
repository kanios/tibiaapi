<?php

namespace AppBundle\Utils;

class AbstractConnector
{
    protected $url;
    protected $param;

    public function __construct(string $param)
    {
        if (empty($param)) return false;

        $this->param = $param;

        return true;
    }

    final public function request() : string
    {
        return file_get_contents($this->getUrl());
    }

    private function getUrl() : string
    {
        return $this->url . '/' . $this->param . '.json';
    }
}
