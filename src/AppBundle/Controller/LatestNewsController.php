<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Utils\LatestNews;

class LatestNewsController extends Controller
{
    public function showAction()
    {
        if ($news = new LatestNews())
            return new Response($news->request());

        return new Response('Error');
    }
}
