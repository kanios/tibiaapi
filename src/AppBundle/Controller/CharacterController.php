<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Utils\Character;

class CharacterController extends Controller
{
    //super komentarz kurwo

    public function showAction($nickname)
    {
        if ($character = new Character($nickname))
            return new Response($character->request());

        return new Response('Error');
    }
}
