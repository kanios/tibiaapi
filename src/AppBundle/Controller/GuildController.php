<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Utils\Guild;

class GuildController extends Controller
{
    public function showAction($name)
    {
        if ($guild = new Guild($name))
            return new Response($guild->request());

        return new Response('Error');
    }
}
